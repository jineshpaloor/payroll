package com.example.payroll.orders;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
class OrderController {

    private final OrderRepository orderRepository;

    OrderController(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    @GetMapping("/orders")
    ResponseEntity<List<Order>> all() {
        List<Order> orders = orderRepository.findAll();
        return ResponseEntity.ok(orders);
    }

    @GetMapping("/orders/{id}")
    ResponseEntity<Optional<Order>> one(@PathVariable Long id) {
        Optional<Order> order = orderRepository.findById(id);
        return ResponseEntity.ok(order);
    }

    @PostMapping("/orders")
    ResponseEntity<Order> newOrder(@RequestBody Order order) {
        order.setStatus(Status.IN_PROGRESS);
        Order newOrder = orderRepository.save(order);
        return ResponseEntity.ok(newOrder);
    }
}
