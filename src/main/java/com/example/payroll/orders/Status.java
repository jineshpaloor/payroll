package com.example.payroll.orders;


public enum Status {

    IN_PROGRESS, //
    COMPLETED, //
    CANCELLED
}