package com.example.payroll.course;

import jakarta.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

// enable the application to find in component scan
@Component
@Transactional
public class CourseCommandLineRunner implements CommandLineRunner {

    private static final Logger log = LoggerFactory.getLogger(CourseCommandLineRunner.class);

    @Autowired
    CourseJdbcRepository repository;

    @Autowired
    CourseJpaRespository jpaRepository;

    @Override
    public void run(String... args) throws Exception {
        log.info("Inserting course record...");
        // jdbc
        repository.insert(new Course(1, "Learn AWS", "Sam"));
        repository.insert(new Course(2, "Learn Docker", "Mat"));
        repository.insert(new Course(3, "Learn Devops", "Ken"));

        // jpa
        jpaRepository.insert(new Course(5, "Learn Golang", "Bam"));
        jpaRepository.insert(new Course(6, "Learn Java", "Col"));
        jpaRepository.insert(new Course(7, "Learn Python", "Ton"));

//        repository.deleteById(1);

        System.out.println(repository.selectById(2));
        System.out.println(repository.selectById(3));
    }
}
